package web

import core.Account
import core.AccountNotFound
import core.Deposit
import core.NewAccount
import core.NotEnoughMoney
import core.NotEnoughMoneyException
import core.Transfer
import core.TransferException
import core.TransferFailed
import core.UpdateAccount
import core.Withdraw
import extensions.newId
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import service.AccountService
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.math.BigDecimal.TEN
import java.math.BigDecimal.ZERO

class AccountControllerImplTest {
    private val ctx = mockk<Context>(relaxed = true)
    private val accountService = mockk<AccountService>(relaxed = true)
    private val controller = AccountControllerImpl(accountService)

    @Test
    fun `POST to create account returns created account`() {
        every { ctx.body<NewAccount>() } returns NewAccount("GBP Acc")
        val createdAccount = Account(newId(), "GBP Acc", ZERO)
        every { accountService.create(any()) } returns createdAccount

        controller.create(ctx)

        verify { ctx.status(201) }
        verify { ctx.json(createdAccount) }
    }

    @Test
    fun `GET to find all accounts returns all present accounts`() {
        val foundAccounts = listOf(Account(newId(), "UAH Acc", ZERO))
        every { accountService.all() } returns foundAccounts

        controller.getAll(ctx)

        verify { ctx.json(foundAccounts) }
    }

    @Test
    fun `GET to find single account returns account for valid id`() {
        val id = newId()
        val foundAccount = Account(id, "UAH Acc", ZERO)
        every { ctx.pathParam("account-id") } returns id
        every { accountService.get(id) } returns foundAccount

        controller.get(ctx)

        verify { ctx.json(foundAccount) }
    }

    @Test
    fun `GET to find single account returns AccountNotFound if there is no account with such id`() {
        val id = newId()
        every { ctx.pathParam("account-id") } returns id
        every { accountService.get(id) } returns null

        controller.get(ctx)

        verify { ctx.status(400) }
        verify { ctx.json(AccountNotFound(id)) }
    }

    @Test
    fun `GET to find single account throws for null id`() {
        every { ctx.pathParam("account-id") } returns ""

        assertThrows<BadRequestResponse> { controller.get(ctx) }
    }

    @Test
    fun `PATCH to update account returns patched account`() {
        val update = UpdateAccount("GBP Acc")
        every { ctx.body<UpdateAccount>() } returns update
        val id = newId()
        every { ctx.pathParam("account-id") } returns id
        val updatedAcc = Account(id, update.name, ZERO)
        every { accountService.update(any(), any()) } returns updatedAcc

        controller.update(ctx)


        verify { ctx.json(updatedAcc) }
    }

    @Test
    fun `PATCH to update account returns AccountNotFound for not existing account`() {
        every { ctx.body<UpdateAccount>() } returns UpdateAccount("GBP Acc")
        val id = newId()
        every { ctx.pathParam("account-id") } returns id
        every { accountService.update(any(), any()) } returns null

        controller.update(ctx)

        verify { ctx.status(400) }
        verify { ctx.json(AccountNotFound(id)) }
    }

    @Test
    fun `DELETE to delete account returns deleted account`() {
        val id = newId()
        every { ctx.pathParam("account-id") } returns id
        val deleted = Account(id, "GBP Acc", ZERO)
        every { accountService.delete(any()) } returns deleted

        controller.delete(ctx)

        verify { ctx.json(deleted) }
    }

    @Test
    fun `DELERE to deleted account returns AccountNotFound for not existing account`() {
        val id = newId()
        every { ctx.pathParam("account-id") } returns id
        every { accountService.delete(any()) } returns null

        controller.delete(ctx)

        verify { ctx.status(400) }
        verify { ctx.json(AccountNotFound(id)) }
    }

    @Test
    fun `PATCH to withdraw returns patched account`() {
        val withdraw = Withdraw(ONE)
        every { ctx.body<Withdraw>() } returns withdraw
        val id = newId()
        every { ctx.pathParam("account-id") } returns id
        val updatedAcc = Account(id, "UAH Acc", ZERO)
        every { accountService.withdraw(any(), any()) } returns updatedAcc

        controller.withdraw(ctx)

        verify { ctx.json(updatedAcc) }
    }

    @Test
    fun `PATCH to withdraw returns NotEnoughMoney if not enough money`() {
        val withdraw = Withdraw(ONE)
        every { ctx.body<Withdraw>() } returns withdraw
        val id = newId()
        every { ctx.pathParam("account-id") } returns id
        every { accountService.withdraw(any(), any()) } throws (NotEnoughMoneyException(id))

        controller.withdraw(ctx)

        verify { ctx.status(400) }
        verify { ctx.json(NotEnoughMoney(id)) }
    }

    @Test
    fun `PATCH to withdraw throws for negative amount`() {
        every { ctx.body<Withdraw>() } returns Withdraw(BigDecimal(-100))
        every { ctx.pathParam("account-id") } returns newId()

        assertThrows<BadRequestResponse> { controller.withdraw(ctx) }
    }


    @Test
    fun `PATCH to deposit returns patched account`() {
        every { ctx.body<Deposit>() } returns Deposit(ONE)
        val id = newId()
        every { ctx.pathParam("account-id") } returns id
        val updatedAcc = Account(id, "UAH Acc", TEN)
        every { accountService.deposit(any(), any()) } returns updatedAcc

        controller.deposit(ctx)

        verify { ctx.json(updatedAcc) }
    }

    @Test
    fun `PATCH to deposit returns AccountNotFound for not existing account`() {
        every { ctx.body<Deposit>() } returns Deposit(ONE)
        val accountId = newId()
        every { ctx.pathParam("account-id") } returns accountId
        every { accountService.deposit(any(), any()) } returns null

        controller.deposit(ctx)

        verify { ctx.status(400) }
        verify { ctx.json(AccountNotFound(accountId)) }
    }

    @Test
    fun `PATCH to deposit throws for negative amount`() {
        every { ctx.body<Deposit>() } returns Deposit(BigDecimal(-100))
        every { ctx.pathParam("account-id") } returns newId()

        assertThrows<BadRequestResponse> { controller.deposit(ctx) }
    }

    @Test
    fun `PATCH to transfer is successful`() {
        every { ctx.body<Transfer>() } returns Transfer(newId(), newId(), ONE)
        every { accountService.transfer(any()) } just Runs

        controller.transfer(ctx)

    }

    @Test
    fun `PATCH to transfer finished with error`() {
        val sourceAccountId = newId()
        val targetAccountId = newId()
        every { ctx.body<Transfer>() } returns Transfer(sourceAccountId, targetAccountId, ONE)

        val transferFailed = TransferFailed(sourceAccountId, targetAccountId)
        every { accountService.transfer(any()) } throws TransferException(sourceAccountId, targetAccountId)

        controller.transfer(ctx)

        verify { ctx.status(400) }
        verify { ctx.json(transferFailed) }
    }


}
