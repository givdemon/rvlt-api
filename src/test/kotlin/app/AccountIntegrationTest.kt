package app

import core.Account
import core.Deposit
import core.NewAccount
import core.NotEnoughMoney
import core.Transfer
import core.Withdraw
import io.javalin.Javalin
import io.javalin.plugin.json.JavalinJson.fromJson
import kong.unirest.HttpResponse
import kong.unirest.Unirest
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.BigDecimal.TEN
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.expect

class AccountIntegrationTest {
    @Test
    fun `account balance as expected after deposit`() {
        val app = startApp(1234)

        val newAccount = createAccount(app)

        assertEquals("GBP Acc", newAccount.name)
        val accountId = newAccount.id
        assertNotNull(accountId)

        val updatedAccountResponse = deposit(app, accountId, BigDecimal(100.50))
        val updatedAccount = fromJson(updatedAccountResponse!!.body, Account::class.java)

        assertEquals(accountId, updatedAccount.id)
        assertEquals(BigDecimal(100.50), updatedAccount.balance)

        app.stop()
    }

    @Test
    fun `withdraw unsuccessful if amount larger than balance`() {
        val app = startApp(1235)

        val account = createAccount(app)

        val updatedAccountResponse = deposit(app, account.id, BigDecimal(100.50))
        val accountAfterDeposit = fromJson(updatedAccountResponse!!.body, Account::class.java)

        val withdrawResponse = Unirest.patch("http://localhost:${app.port()}/accounts/{account-id}/withdraw")
            .header("Content-Type", "application/json")
            .routeParam("account-id", account.id)
            .body(Withdraw(accountAfterDeposit.balance * TEN))
            .asString()

        val notEnoughMoneyResponse = fromJson(withdrawResponse!!.body, NotEnoughMoney::class.java)
        assertEquals(account.id, notEnoughMoneyResponse.accountId)
        app.stop()
    }

    @Test
    fun `transfer is successful`() {
        val app = startApp(1236)

        val sourceAccount = createAccount(app)
        val targetAccount = createAccount(app)

        deposit(app, sourceAccount.id, BigDecimal(100))

        val transferResponse = Unirest.patch("http://localhost:${app.port()}/transfer")
            .header("Content-Type", "application/json")
            .body(Transfer(sourceAccount.id, targetAccount.id, BigDecimal(10)))
            .asString()

        assertEquals(200, transferResponse.status)

        val accountsAfterTransfer = Unirest.get("http://localhost:${app.port()}/accounts")
            .header("Content-Type", "application/json")
            .asJson()
            .body.array.map { fromJson(it.toString(), Account::class.java) }

        expect(BigDecimal(90)) {
            accountsAfterTransfer.first { it.id == sourceAccount.id }.balance
        }
        expect(BigDecimal(10)) {
            accountsAfterTransfer.first { it.id == targetAccount.id }.balance
        }
        app.stop()
    }

    @Test
    fun `transfer is invalid if amount less than zero`() {
        val app = startApp(1237)

        val sourceAccount = createAccount(app)
        val targetAccount = createAccount(app)

        val transferResponse = Unirest.patch("http://localhost:${app.port()}/transfer")
            .header("Content-Type", "application/json")
            .body(Transfer(sourceAccount.id, targetAccount.id, BigDecimal(-100)))
            .asString()

        assertEquals(400, transferResponse.status)
        assertEquals("Bad Request", transferResponse.statusText)
        app.stop()
    }

    private fun deposit(app: Javalin, accountId: String, amount: BigDecimal): HttpResponse<String>? {
        return Unirest.patch("http://localhost:${app.port()}/accounts/{account-id}/deposit")
            .header("Content-Type", "application/json")
            .routeParam("account-id", accountId)
            .body(Deposit(amount))
            .asString()
    }

    private fun createAccount(app: Javalin): Account {
        val newAccountResponse = Unirest.post("http://localhost:${app.port()}/accounts")
            .header("Content-Type", "application/json")
            .body(NewAccount("GBP Acc"))
            .asString()
        return fromJson(newAccountResponse.body, Account::class.java)
    }

}
