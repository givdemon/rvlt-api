package database

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class AccountRepositoryImplTest {

    private val dao = AccountRepositoryImpl()

    @Test
    fun `account saved`() {
        val expectedName = "GBP  Account"
        val savedAccount = dao.save(expectedName)
        assertEquals(savedAccount.name, expectedName)
        assertNotNull(savedAccount.id)
    }

    @Test
    fun `account found by id`() {
        val savedAccount = dao.save("GBP  Account")
        val foundAccount = dao.findById(savedAccount.id)
        assertEquals(savedAccount, foundAccount)
    }

    @Test
    fun `account updated`() {
        val savedAccount = dao.save("GBP  Account")
        dao.update(savedAccount.copy(balance = BigDecimal(120)))
        val foundAccount = dao.findById(savedAccount.id)
        assertEquals(foundAccount!!.balance, BigDecimal(120))
    }

    @Test
    fun `account deleted`() {
        val savedAccount = dao.save("GBP  Account")
        val deletedAccount = dao.delete(savedAccount.id)
        assertEquals(savedAccount, deletedAccount!!)
    }
}
