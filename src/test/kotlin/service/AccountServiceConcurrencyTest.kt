package service

import core.NewAccount
import core.Transfer
import database.AccountRepositoryImpl
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.BigDecimal.ONE
import java.math.BigDecimal.ZERO
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import kotlin.test.assertEquals


class AccountServiceConcurrencyTest {

    @Test
    fun `concurrent deposit is successful`() {
        val service = AccountServiceImpl(AccountRepositoryImpl())
        val account = service.create(NewAccount("cnc"))

        executeConcurrently(
            1_000,
            1_000
        ) { service.deposit(account.id, ONE) }

        assertEquals(BigDecimal(1_000_000), service.get(account.id)!!.balance)
    }

    @Test
    fun `concurrent withdraw is successful`() {
        val service = AccountServiceImpl(AccountRepositoryImpl())
        val account = service.create(NewAccount("cnc"))
        service.deposit(account.id, BigDecimal(2_000_000))

        executeConcurrently(
            1_000,
            2_000
        ) { service.withdraw(account.id, ONE) }

        assertEquals(ZERO, service.get(account.id)!!.balance)
    }

    @Test
    fun `concurrent transfer is successful`() {
        val service = AccountServiceImpl(AccountRepositoryImpl())
        val sourceAccount = service.create(NewAccount("source"))
        val targetAccount = service.create(NewAccount("target"))
        service.deposit(sourceAccount.id, BigDecimal(2_000_000))

        executeConcurrently(
            1_000,
            2_000
        ) { service.transfer(Transfer(sourceAccount.id, targetAccount.id, ONE)) }

        assertEquals(ZERO, service.get(sourceAccount.id)!!.balance)
    }

    private fun <R> executeConcurrently(
        threadCount: Int,
        iterations: Int,
        block: () -> R
    ) {
        val latch = CountDownLatch(threadCount)
        val executor = Executors.newFixedThreadPool(threadCount)
        for (i in 0 until threadCount) {
            executor.execute(Runnable {
                try {
                    for (j in 0 until iterations) {
                        block()
                    }
                } finally {
                    latch.countDown()
                }
            })
        }
        latch.await()
        executor.shutdown()

    }

}
