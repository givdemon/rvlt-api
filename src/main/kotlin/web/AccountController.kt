package web

import io.javalin.http.Context

interface AccountController {
    fun create(ctx: Context)
    fun getAll(ctx: Context)
    fun get(ctx: Context)
    fun update(ctx: Context)
    fun delete(ctx: Context)
    fun withdraw(ctx: Context)
    fun deposit(ctx: Context)
    fun transfer(ctx: Context)
}
