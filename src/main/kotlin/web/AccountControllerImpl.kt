package web

import core.Account
import core.AccountNotFound
import core.Deposit
import core.NewAccount
import core.NotEnoughMoney
import core.NotEnoughMoneyException
import core.Transfer
import core.TransferException
import core.TransferFailed
import core.UpdateAccount
import core.Withdraw
import extensions.getIdOrThrow
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import mu.KotlinLogging
import service.AccountService
import java.math.BigDecimal.ZERO

class AccountControllerImpl(private val service: AccountService) : AccountController {
    private val logger = KotlinLogging.logger {}

    override fun create(ctx: Context) {
        val newAccount = ctx.body<NewAccount>()
        logger.debug { "received: $newAccount" }
        val createAccount = service.create(newAccount)
        logger.debug { "created account: $createAccount" }
        ctx.json(createAccount)
        ctx.status(201)
    }

    override fun getAll(ctx: Context) {
        val accounts = service.all()
        logger.debug { "found account: $accounts" }
        ctx.json(accounts)
    }

    override fun get(ctx: Context) {
        val id = ctx.accountId()
        val account = service.get(id)
        logger.debug { "for $id found account: $account" }
        ctx.addAccountResponse(account, id)
    }

    override fun update(ctx: Context) {
        val id = ctx.accountId()
        val update = ctx.body<UpdateAccount>()
        logger.debug { "received: $update" }
        val account = service.update(id, update)
        logger.debug { "updated account: $account" }
        ctx.addAccountResponse(account, id)
    }

    override fun delete(ctx: Context) {
        val id = ctx.accountId()
        val deleted = service.delete(id)
        logger.debug { "for $id deleted account: $deleted" }
        ctx.addAccountResponse(deleted, id)
    }

    override fun withdraw(ctx: Context) {
        val id = ctx.accountId()
        val withdraw = ctx.body<Withdraw>()
        logger.debug { "received: $withdraw" }
        if (withdraw.amount < ZERO) {
            logger.error { "withdraw amount ${withdraw.amount} is negative" }
            throw BadRequestResponse()
        }
        val updated = try {
            service.withdraw(id, withdraw.amount)
        } catch (e: NotEnoughMoneyException) {
            ctx.json(NotEnoughMoney(id))
            ctx.status(400)
            return
        }
        logger.debug { "account after withdraw ${withdraw.amount}: $updated" }
        ctx.addAccountResponse(updated, id)
    }

    override fun deposit(ctx: Context) {
        val id = ctx.accountId()
        val deposit = ctx.body<Deposit>()
        logger.debug { "received: $deposit" }
        if (deposit.amount < ZERO) {
            logger.error { "deposit amount ${deposit.amount} is negative" }
            throw BadRequestResponse()
        }
        val updatedAccount = service.deposit(id, deposit.amount)
        logger.debug { "account after deposit ${deposit.amount}: $updatedAccount" }
        ctx.addAccountResponse(updatedAccount, id)
    }

    override fun transfer(ctx: Context) {
        val transfer = ctx.body<Transfer>()
        logger.debug { "received: $transfer" }

        if (transfer.amount < ZERO) {
            logger.error { "transfer amount ${transfer.amount} is negative" }
            throw BadRequestResponse()
        }
        try {
            service.transfer(transfer)
            logger.debug { "transfer was successful" }
        } catch (e: TransferException) {
            logger.error(e) { "failed to perform transfer" }
            ctx.json(TransferFailed(transfer.sourceAccountId, transfer.targetAccountId))
            ctx.status(400)
            return
        }
    }

    private fun Context.addAccountResponse(account: Account?, id: String) {
        if (account == null) {
            json(AccountNotFound(id))
            status(400)
        } else {
            json(account)
        }
    }

    private fun Context.accountId(): String {
        return getIdOrThrow("account-id")
    }

}
