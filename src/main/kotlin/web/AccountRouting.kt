package web

import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.delete
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.patch
import io.javalin.apibuilder.ApiBuilder.post

class AccountRouting(
    private val javalin: Javalin,
    private val accountController: AccountController
) {
    fun bindRoutes() {
        javalin.routes {

            get("/accounts") { accountController.getAll(it) }

            get("/accounts/:account-id") { accountController.get(it) }

            post("/accounts") { accountController.create(it) }

            patch("/accounts/:account-id") { accountController.update(it) }

            delete("/accounts/:account-id") { accountController.delete(it) }

            patch("/accounts/:account-id/withdraw") { accountController.withdraw(it) }

            patch("/accounts/:account-id/deposit") { accountController.deposit(it) }

            patch("/transfer") { accountController.transfer(it) }

        }
    }
}
