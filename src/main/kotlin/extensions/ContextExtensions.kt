package extensions

import io.javalin.http.BadRequestResponse
import io.javalin.http.Context

fun Context.getIdOrThrow(idParam: String): String {
    val id = pathParam(idParam)
    if (id.isBlank()) {
        throw BadRequestResponse()
    }
    return id
}
