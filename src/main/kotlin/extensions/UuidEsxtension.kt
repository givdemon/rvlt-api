package extensions

import java.util.UUID

fun newId() = UUID.randomUUID().toString()
