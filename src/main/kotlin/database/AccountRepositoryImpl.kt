package database

import core.Account
import mu.KotlinLogging
import java.math.BigDecimal
import java.util.UUID

class AccountRepositoryImpl : AccountRepository {
    private val logger = KotlinLogging.logger {}
    private val accounts = HashMap<String, Account>()

    override fun save(name: String): Account {
        val id = UUID.randomUUID().toString()
        val account = Account(id, name, BigDecimal.ZERO)
        accounts[id] = account
        logger.debug { "saved account: $account" }
        return account
    }

    override fun all() = accounts.values

    override fun findById(id: String): Account? {
        return accounts[id]
    }

    override fun update(account: Account) {
        accounts[account.id] = account
        logger.debug { "updated account: $account" }
    }

    override fun delete(id: String): Account? {
        val removed = accounts.remove(id)
        logger.debug { "deleted account: $removed" }
        return removed
    }
}
