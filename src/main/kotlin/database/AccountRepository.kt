package database

import core.Account

interface AccountRepository {
    fun save(name: String): Account
    fun all(): Collection<Account>
    fun findById(id: String): Account?
    fun update(account: Account)
    fun delete(id: String): Account?
}
