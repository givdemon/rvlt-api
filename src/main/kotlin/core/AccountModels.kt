package core

import java.math.BigDecimal

data class Account(
    val id: String,
    val name: String,
    val balance: BigDecimal
)

data class NewAccount(val name: String)

data class UpdateAccount(val name: String)

data class AccountNotFound(val id: String)

data class NotEnoughMoney(val accountId: String)

data class Withdraw(val amount: BigDecimal)

data class Deposit(val amount: BigDecimal)

data class Transfer(val sourceAccountId: String, val targetAccountId: String, val amount: BigDecimal)

data class TransferFailed(
    val sourceAccountId: String,
    val targetAccountId: String
)

class NotEnoughMoneyException(val accountId: String) : Exception()

class TransferException(
    private val sourceId: String,
    private val targetId: String,
    private val reason: TransferFailedReason? = null
) : Exception("Transfer from  $sourceId to $targetId failed. Reason: $reason")

enum class TransferFailedReason {
    NOT_ENOUGH_MONEY, ACCOUNT_NOT_FOUND
}
