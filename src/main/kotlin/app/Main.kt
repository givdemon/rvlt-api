package app

import database.AccountRepositoryImpl
import io.javalin.Javalin
import service.AccountServiceImpl
import web.AccountControllerImpl
import web.AccountRouting

fun main(args: Array<String>) {
    startApp(7000)
}

fun startApp(port: Int): Javalin {
    val app = Javalin.create().start(port)
    injectDependencies(app)
    return app
}

private fun injectDependencies(app: Javalin) {
    val accountRouting = AccountRouting(
        app,
        AccountControllerImpl(
            AccountServiceImpl(
                AccountRepositoryImpl()
            )
        )
    )
    accountRouting.bindRoutes()
}
