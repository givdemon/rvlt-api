package service

import core.Account
import core.NewAccount
import core.Transfer
import core.UpdateAccount
import java.math.BigDecimal

interface AccountService {
    fun create(newAccount: NewAccount): Account
    fun all(): Collection<Account>
    fun get(id: String): Account?
    fun update(id: String, update: UpdateAccount): Account?
    fun delete(id: String): Account?
    /**
     * Throws NotEnoughMoneyException if amount > balance
     */
    fun withdraw(id: String, amount: BigDecimal): Account?
    fun deposit(id: String, amount: BigDecimal): Account?
    /**
     * Throws TransferFailed with corresponding reason
     */
    fun transfer(transfer: Transfer)
}
