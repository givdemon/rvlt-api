package service

import core.Account
import core.NewAccount
import core.NotEnoughMoneyException
import core.Transfer
import core.TransferException
import core.TransferFailedReason
import core.TransferFailedReason.ACCOUNT_NOT_FOUND
import core.TransferFailedReason.NOT_ENOUGH_MONEY
import core.UpdateAccount
import database.AccountRepository
import mu.KotlinLogging
import java.math.BigDecimal

class AccountServiceImpl(private val repository: AccountRepository) : AccountService {

    private val logger = KotlinLogging.logger {}

    override fun create(newAccount: NewAccount): Account {
        return repository.save(newAccount.name)
    }

    @Synchronized
    override fun all(): Collection<Account> {
        return repository.all()
    }

    @Synchronized
    override fun get(id: String): Account? {
        return repository.findById(id)
    }

    @Synchronized
    override fun update(id: String, update: UpdateAccount): Account? {
        return get(id)?.run {
            val newAccount = copy(name = update.name)
            repository.update(newAccount)
            newAccount
        }
    }

    @Synchronized
    override fun delete(id: String): Account? {
        return repository.delete(id)
    }

    @Synchronized
    override fun withdraw(id: String, amount: BigDecimal): Account? {
        return get(id)?.run {
            if (balance < amount) {
                logger.error { "Account $id balance is less then withdraw amount: $balance < $amount " }
                throw NotEnoughMoneyException(id)
            }
            val newBalance = balance - amount
            val newAccount = copy(balance = newBalance)
            repository.update(newAccount)
            newAccount
        }
    }

    @Synchronized
    override fun deposit(id: String, amount: BigDecimal): Account? {
        return get(id)?.run {
            val newBalance = balance + amount
            val newAccount = copy(balance = newBalance)
            repository.update(newAccount)
            newAccount
        }
    }

    @Synchronized
    override fun transfer(transfer: Transfer) {
        val source = get(transfer.sourceAccountId) ?: throw transfer.toException(ACCOUNT_NOT_FOUND)
        val target = get(transfer.targetAccountId) ?: throw transfer.toException(ACCOUNT_NOT_FOUND)
        if (source.balance < transfer.amount) {
            throw transfer.toException(NOT_ENOUGH_MONEY)
        }

        val sourceNewBalance = source.balance - transfer.amount
        repository.update(source.copy(balance = sourceNewBalance))

        val targetNewBalance = target.balance + transfer.amount
        repository.update(target.copy(balance = targetNewBalance))
    }

    private fun Transfer.toException(reason: TransferFailedReason): TransferException {
        return TransferException(
            sourceAccountId,
            targetAccountId,
            reason
        )
    }
}
