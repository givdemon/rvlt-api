# Revolut Account Transfer API

API contains Account CRUD endpoints, money withdraw, transfer and deposit andpoints.

## TODO
1. Implement Counterparties for transfers and payments.
2. Move money operations in separate module.
2. Use [Thread-weaver](https://github.com/google/thread-weaver) for better concurrent tests.
3. Use some ACID DB instead of a HashMap.
4. Use [Logback](http://logback.qos.ch/) for better logging.

## License
[MIT](https://choosealicense.com/licenses/mit/)
